const GtfsData = require('@ltbuses/gtfs-parser/GtfsData');
const rawBusData = require('./bus_data');
const express = require('express');
const serverless = require('serverless-http');
const fetch = require('node-fetch');
const moment = require('moment-timezone');

const gtfs = new GtfsData({
  holidays: new Map(rawBusData.holidays),
  schedules: new Map(rawBusData.schedules),
  stops: new Map(rawBusData.stops),
  routes: new Map(rawBusData.routes),
});

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  next();
});

app.get('/routes', (req, res) => {
  res.json(gtfs.getRoutes());
});

app.get('/stops', (req, res) => {
  let stops = gtfs.getStops()
    .filter(stop => stop.routes.size > 0)
    .filter(stop => stop.number)
    .sort(({number: a}, {number: b}) => a - b)
    .map(stop => ({
      id: stop.id,
      number: stop.number,
      name: stop.name.split(' - ').shift(),
      lat: stop.lat,
      lon: stop.lon,
      routes: [...stop.routes.keys()].map(k => `${k}`),
    }));

  if (req.query.route) {
    const {route} = req.query;
    stops = stops.filter(({routes}) => routes.includes(route));
  }

  if (req.query.limit) {
    const {limit: rawLimit, offset: rawOffset = '0'} = req.query;
    const limit = Number.parseInt(rawLimit, 10);
    const offset = Number.parseInt(rawOffset, 10);
    stops = stops.slice(offset, offset + limit);
  }

  if (req.query.simple) {
    stops = stops.map(stop => ({
      number: stop.number,
      name: stop.name,
      routes: stop.routes,
    }));
  }

  res.json(stops);
});

app.get('/stops/:stopNumber/:routeNumber?', (req, res) => {
  const stopNumber = parseInt(req.params.stopNumber, 10);

  // String to int to string gets rid of leading 0s
  const routeNumber = `${parseInt(req.params.routeNumber, 10)}`;

  const stop = gtfs.getStops()
    .find(s => parseInt(s.number, 10) === stopNumber);

  if (!stop) {
    res.status(404).send('Stop unkown or not in service');
    return;
  }

  const realtimeUrl = 'https://realtime.londontransit.ca/InfoWeb';
  const requestOptions = {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({
      method: 'GetBusTimes',
      params: {
        LinesRequest: {
          Client: 'MobileWeb',
          GetStopTimes: 1,
          GetStopTripInfo: 0,
          NumStopTimes: Math.max(stop.routes.size * 5, 200),
          Radius: 0,
          StopId: stop.id,
          SuppresLinesUnloadOnly: 0,
        },
      },
      version: '1.1',
    }),
  };

  const fixTime = (time) => {
    const totalMinutes = Math.floor(time / 60);
    const hours = Math.floor(totalMinutes / 60);
    let minutes = totalMinutes % 60;

    if (minutes < 10) {
      minutes = `0${minutes}`;
    }

    // Afternoon.
    if (hours > 12) {
      return `${hours - 12}:${minutes}pm`;
    }

    // Midnight.
    if (hours === 0) {
      return `12:${minutes}am`;
    }

    // Morning.
    return `${hours}:${minutes}am`;
  };

  // Sometimes the API returns duplicate entries for each trip.
  const trips = [];
  const filterTrips = ({TripId}) => {
    if (trips.includes(TripId)) {
      return false;
    }
    trips.push(TripId);
    return true;
  };

  const now = moment.tz('America/Toronto');

  fetch(realtimeUrl, requestOptions)
    .then(response => response.json())
    .then((response) => {
      let times = [];
      if (response.result && response.result.length > 0) {
        const result = response.result[0];
        if (result.StopTimeResult && result.StopTimeResult.length > 0) {
          const {
            StopTimesOutput: timeOutput,
          } = result.StopTimeResult[0];
          times = timeOutput
            .filter(filterTrips)
            .sort(({ETime: timeA, Date: dateA}, {ETime: timeB, Date: dateB}) => {
              const a = parseInt(`${dateA}${timeA}`, 10);
              const b = parseInt(`${dateB}${timeB}`, 10);
              return a - b;
            })
            .map((data) => {
              const time = data.TimeOutput ? `${data.TimeOutput.replace(/\(s\)/i, '')}m` : fixTime(data.ETime);
              const arrival = moment(time, 'h:mma').tz('America/Toronto', true);
              const wait = moment.duration(arrival.diff(now));
              return {
                destination: data.DestinationSign,
                route: `${parseInt(data.LineAbbr, 10)}`,
                direction: data.DirectionName.trim(),
                trip: data.TripId,
                time: time,
                wait: {
                  hours: wait.hours(),
                  minutes: wait.minutes() % 60,
                  readable: wait.humanize(false),
                },
              };
            });
        }
      }

      // Yes, 'NaN'. See the "string to int to string" conversion above.
      if (routeNumber === 'NaN') {
        res.json(times);
        return;
      }

      res.json(times.filter(({route}) => route === routeNumber));
    });
});

module.exports.api = serverless(app);
